//
//  SearchMovieViewModel.swift
//  MobilliumCase-2
//
//  Created by Ali Şengür on 3.10.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import Foundation


final class SearchMovieViewModel {
    
    var searchedMovies: [Movie] = [] {  // update UI when changed prop
        didSet {
            self.updateUI!()
        }
    }
    
    
    var reloadData: (() -> Void)?
    var updateUI: (() -> Void)?
    
    
    //MARK: - search movie with query
    func searchMovies(query: String) {
        APIService.shared.searchMovies(query: query) { [weak self] result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let movies):
                self?.searchedMovies = movies
            }
        }
    }
}
