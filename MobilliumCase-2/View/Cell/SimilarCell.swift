//
//  SimilarCell.swift
//  MobilliumCase-2
//
//  Created by Ali Şengür on 4.10.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit

class SimilarCell: UICollectionViewCell {
    
    
    //MARK: - outlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    
    //MARK: - configure cell
    public func configure(with movie: Movie) {
        guard let path = movie.posterPath else {
            return
        }
        let posterPath = "https://image.tmdb.org/t/p/original\(path)"
        let url = URL(string: posterPath)
        
        DispatchQueue.main.async {
            self.imageView.kf.setImage(with: url)
            self.titleLabel.text = movie.title
            self.dateLabel.text = "(\(movie.yearText))"
        }
    }
}
