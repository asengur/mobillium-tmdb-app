//
//  APIService.swift
//  MobilliumCase-2
//
//  Created by Ali Şengür on 2.10.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import Foundation
import Alamofire


enum MovieError: Error {
    case noDataAvailable
    case canNotProcessData
}




class APIService {
    
    let API_KEY = "0a7a0796c394196ba34dd4aa37ba190e"
    
    // singleton
    static let shared = APIService()
    
    
    
    //MARK: - get now playing movies from tmdb api
    func getNowPlayingMovies(completion: @escaping(Result<[Movie], MovieError>) -> Void) {
        let resourceUrl = "https://api.themoviedb.org/3/movie/now_playing?api_key=\(API_KEY)"
        AF.request(resourceUrl, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseData { response in
            
            guard let json = response.data else {
                completion(.failure(.noDataAvailable))
                return
            }

            do {
                let decoder = JSONDecoder()
                let moviesResponse = try decoder.decode(MovieResult.self, from: json)
                let nowPlayingMovieDetails = moviesResponse.movies ?? []
                completion(.success(nowPlayingMovieDetails))
            } catch {
                completion(.failure(.canNotProcessData))
            }
        }
    }
    
    
    
    //MARK: - get upcoming movies from tmdb api
    func getUpcomingMovies(completion: @escaping(Result<[Movie], MovieError>) -> Void) {
        let resourceUrl = "https://api.themoviedb.org/3/movie/upcoming?api_key=\(API_KEY)"
        AF.request(resourceUrl, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseData { response in
            
            guard let json = response.data else {
                completion(.failure(.noDataAvailable))
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let movieResponse = try decoder.decode(MovieResult.self, from: json)
                let upcomingMovieDetails = movieResponse.movies ?? []
                completion(.success(upcomingMovieDetails))
            } catch {
                completion(.failure(.canNotProcessData))
            }
        }
    }
    
    
    //MARK: - search movies with query text
    func searchMovies(query: String, completion: @escaping(Result<[Movie], MovieError>) -> Void) {
        let resourceUrl = "https://api.themoviedb.org/3/search/movie?api_key=\(API_KEY)&query=\(query)"
        print("url : \(resourceUrl)")
        AF.request(resourceUrl, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseData { response in
            
            guard let json = response.data else {
                completion(.failure(.noDataAvailable))
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let movieResponse = try decoder.decode(MovieResult.self, from: json)
                let searchedMovieDetails = movieResponse.movies ?? []
                completion(.success(searchedMovieDetails))
            } catch {
                completion(.failure(.canNotProcessData))
            }
        }
    }
    
    
    
    
    
    //MARK: - get movie details with movie id
    func getMovieFromId(id: Int, completion: @escaping(Result<Movie, MovieError>) -> Void) {
        let resourceUrl = "https://api.themoviedb.org/3/movie/\(id)?api_key=\(API_KEY)"
        AF.request(resourceUrl, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseData { response in
            guard let json = response.data else {
                completion(.failure(.noDataAvailable))
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let movieResponse = try decoder.decode(Movie.self, from: json)
                let movie = movieResponse
                completion(.success(movie))
            } catch {
                completion(.failure(.canNotProcessData))
            }
        }
    }
    
    
    
    //MARK: - get similar movies with movie id
    func getSimilarMovies(id: Int, completion: @escaping(Result<[Movie], MovieError>) -> Void) {
        let resourceUrl = "https://api.themoviedb.org/3/movie/\(id)/similar?api_key=\(API_KEY)"
        AF.request(resourceUrl, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseData { response in
            
            guard let json = response.data else {
                completion(.failure(.noDataAvailable))
                return
            }

            do {
                let decoder = JSONDecoder()
                let moviesResponse = try decoder.decode(SimilarMovies.self, from: json)
                let similarMovies = moviesResponse.similarMovies ?? []
                completion(.success(similarMovies))
            } catch {
                completion(.failure(.canNotProcessData))
            }
        }
    }
}
