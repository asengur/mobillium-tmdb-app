Bu projede MVVM tasarım desenini ilk defa kullandım.

Kullanılanlar: 

Alamofire, 
Kingfisher, 
Collection View, 
Table View,
Page Control,
Search Bar,
Safari Services

![home.png](https://bitbucket.org/asengur/mobillium-tmdb-app/raw/master/MobilliumCase-2/Screenshots/home.png)
![detail.png](https://bitbucket.org/asengur/mobillium-tmdb-app/raw/master/MobilliumCase-2/Screenshots/detail.png)
![search.png](https://bitbucket.org/asengur/mobillium-tmdb-app/raw/master/MobilliumCase-2/Screenshots/search.png)
![imdbPage.png](https://bitbucket.org/asengur/mobillium-tmdb-app/raw/master/MobilliumCase-2/Screenshots/imdbPage.png)