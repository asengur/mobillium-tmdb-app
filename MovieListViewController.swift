//
//  MovieListViewController.swift
//  MobilliumCase-2
//
//  Created by Ali Şengür on 2.10.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit

class MovieListViewController: UIViewController {

    //MARK: - outlets
    @IBOutlet weak var collectionView: UICollectionView! /// now playing movies
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView! /// upcoming movies
    @IBOutlet weak var searchTableView: UITableView! /// searched movies
    
    
    
    //MARK: - properties
    let nowPlayingViewModel = NowPlayingViewModel()
    let upcomingViewModel = UpcomingViewModel()
    let searchViewModel = SearchMovieViewModel()
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.navigationItem.titleView = searchBar // configure search bar
        searchBar.delegate = self
        
        collectionView.delegate = self  // configure collection view (now playing movies)
        collectionView.dataSource = self
        nowPlayingViewModel.getMovies()
        nowPlayingViewModel.reloadData = {
            self.collectionView.reloadData()
        }
        
        
        setupUpcomingTableViewCell()  // configure table view (upcoming movies)
        upcomingViewModel.getMovies()
        upcomingViewModel.reloadData = {
            self.tableView.reloadData()
        }
        
        
        searchTableView.delegate = self   // configure table view (searching movies)
        searchTableView.dataSource = self
        searchTableView.isHidden = true
    }
    
    
    
    fileprivate func setupUpcomingTableViewCell() {
        tableView.delegate = self
        tableView.dataSource = self
        let cellNib = UINib(nibName: "UpcomingCell", bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: "UpcomingCell")
    }
}




//MARK: - Collection View Functions

extension MovieListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let count = nowPlayingViewModel.nowPlayingMovies.count
        pageControl.numberOfPages = count  ///  number of item in page control
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NowPlayingCell", for: indexPath) as! NowPlayingCell
        let movie = nowPlayingViewModel.nowPlayingMovies[indexPath.row]
        cell.configure(with: movie)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let detailsVC = self.storyboard?.instantiateViewController(identifier: "MovieDetailViewController") as? MovieDetailViewController {
            guard let id = nowPlayingViewModel.nowPlayingMovies[indexPath.row].id else {
                return
            }
            detailsVC.movieId = id
            self.navigationController?.pushViewController(detailsVC, animated: true)
        }
    }

    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let witdh = scrollView.frame.width - (scrollView.contentInset.left*2)
        let index = scrollView.contentOffset.x / witdh
        let roundedIndex = round(index)
        self.pageControl?.currentPage = Int(roundedIndex)
    }
    

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }

    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        pageControl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
}






//MARK: - Collection View Flow Layout

extension MovieListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let frameSize = collectionView.frame.size
        return CGSize(width: frameSize.width - 10, height: frameSize.height)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
}





//MARK: - Table View Functions

extension MovieListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableView { /// for upcoming movies
            return upcomingViewModel.upcomingMovies.count
        } else { /// for searched movies
            return searchViewModel.searchedMovies.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpcomingCell", for: indexPath) as! UpcomingCell
            let movie = upcomingViewModel.upcomingMovies[indexPath.row]
            cell.configure(with: movie)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath)
            let movie = searchViewModel.searchedMovies[indexPath.row]
            cell.textLabel?.text = "\(movie.title!)(\(movie.yearText))"
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tableView {
            if let detailsVC = self.storyboard?.instantiateViewController(identifier: "MovieDetailViewController") as? MovieDetailViewController {
                guard let id = upcomingViewModel.upcomingMovies[indexPath.row].id else {
                    return
                }
                detailsVC.movieId = id
                self.navigationController?.pushViewController(detailsVC, animated: true)
            }
        } else {
            if let detailsVC = self.storyboard?.instantiateViewController(identifier: "MovieDetailViewController") as? MovieDetailViewController {
                guard let id = searchViewModel.searchedMovies[indexPath.row].id else {
                    return
                }
                detailsVC.movieId = id
                self.navigationController?.pushViewController(detailsVC, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.tableView {
            return 150
        } else {
            return 43.5
        }
    }
}




//MARK: - Search Bar Functions

extension MovieListViewController: UISearchBarDelegate {
    
    // works when entered or deleted text in the search bar
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let editedText = searchText.replacingOccurrences(of: " ", with: "+")
        
        searchViewModel.searchMovies(query: editedText) // search movies 
        searchViewModel.updateUI = {
            self.updateUI()
        }
    }
    
    
    // update UI
    func updateUI() {
        if searchViewModel.searchedMovies.isEmpty {
            self.searchTableView.isHidden = true
            self.collectionView.isHidden = false
            self.tableView.isHidden = false
        } else {
            self.searchTableView.isHidden = false
            self.collectionView.isHidden = true
            self.tableView.isHidden = true
            self.searchTableView.reloadData()
        }
    }

}
