//
//  MovieDetailViewController.swift
//  MobilliumCase-2
//
//  Created by Ali Şengür on 4.10.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit
import Kingfisher
import SafariServices


class MovieDetailViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var voteAverageLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var releaseLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    //MARK: - Properties
    var movieId: Int?
    let movieDetailViewModel = MovieDetailViewModel()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getMovieFromId(id: movieId!)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        getSimilarMovies(id: movieId!)
    }
    
    
    
    private func getMovieFromId(id: Int) {
        movieDetailViewModel.getMovieFromId(id: id)
        movieDetailViewModel.setupUI = {
            self.setupUI()
        }
    }
    
    
    
    func setupUI() {
        guard let movie = movieDetailViewModel.movie else { return }
        guard let backdropPath = movieDetailViewModel.movie?.backdropPath else { return }
        let path = "https://image.tmdb.org/t/p/original\(backdropPath)"
        print(path)
        let url = URL(string: path)

        DispatchQueue.main.async {
            self.imageView.kf.setImage(with: url)
            self.titleLabel.text = movie.title
            self.overviewLabel.text = movie.overview
            self.releaseLabel.text = movie.releaseText
            self.voteAverageLabel.text = "\(movie.voteAverage!)"
            self.ratingLabel.text = movie.ratingText
        }
    }
    
    
    
    private func getSimilarMovies(id: Int) {
        movieDetailViewModel.getSimilarMovies(id: id)
        movieDetailViewModel.reloadData = {
            self.collectionView.reloadData()
        }
    }
    
    
    
    
    //MARK: - Open IMDB page for movie
    
    @IBAction func openIMDBPage(_ sender: UIButton) {
        guard let imdbID = movieDetailViewModel.movie?.imdbId else { return }
        let urlString = "https://www.imdb.com/title/\(imdbID)/"
        print(urlString)
        guard let url = URL(string: urlString) else { return }
        let safariVC = SFSafariViewController(url: url)
        present(safariVC, animated: true)

    }
}



extension MovieDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let similarMovies = movieDetailViewModel.similarMovies {
            return similarMovies.count
        }
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "SimilarCell", for: indexPath) as! SimilarCell
        guard let similarMovies = movieDetailViewModel.similarMovies else {
            return UICollectionViewCell()
        }
        cell.imageView.layer.cornerRadius = 4
        cell.configure(with: similarMovies[indexPath.row])
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let detailsVC = self.storyboard?.instantiateViewController(identifier: "MovieDetailViewController") as? MovieDetailViewController {
            guard let similarMovies = movieDetailViewModel.similarMovies else {
                return
            }
            detailsVC.movieId = similarMovies[indexPath.row].id
            self.navigationController?.pushViewController(detailsVC, animated: true)
        }
    }
    
    
}
